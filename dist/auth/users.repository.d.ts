import { Repository } from 'typeorm';
import { User } from './user.entity';
export declare class UsersRepository extends Repository<User> {
    createUser(username: string, email: string, password: string): Promise<void>;
    updateUserSpotifyToken(id: string, token: string): Promise<any>;
    findOneByEmail(email: string): Promise<User>;
    findOneById(id: string): Promise<User>;
    findByPayload({ email }: any): Promise<User>;
}
