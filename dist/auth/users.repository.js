"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersRepository = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const user_entity_1 = require("./user.entity");
let UsersRepository = class UsersRepository extends typeorm_1.Repository {
    async createUser(username, email, password) {
        const user = this.create({ username, email, password });
        try {
            await this.save(user);
        }
        catch (e) {
            if (e.code === 'ER_DUP_ENTRY') {
                throw new common_1.ConflictException('Error in registration');
            }
            throw new common_1.InternalServerErrorException();
        }
    }
    async updateUserSpotifyToken(id, token) {
        try {
            await this.update(id, { spotifyToken: token.toString() });
        }
        catch (e) {
            if (e.code === 'ER_DUP_ENTRY') {
                throw new common_1.ConflictException('Error saving user data');
            }
            throw new common_1.InternalServerErrorException();
        }
        const user = await this.findOneById(id);
        return user;
    }
    async findOneByEmail(email) {
        return await this.findOne({ email });
    }
    async findOneById(id) {
        return await this.findOne({ id });
    }
    async findByPayload({ email }) {
        return await this.findOne({
            where: { email }
        });
    }
};
UsersRepository = __decorate([
    (0, typeorm_1.EntityRepository)(user_entity_1.User)
], UsersRepository);
exports.UsersRepository = UsersRepository;
//# sourceMappingURL=users.repository.js.map