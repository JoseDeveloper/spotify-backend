import { JwtService } from '@nestjs/jwt';
import { LoginDto } from './dto/login.dto';
import { RegisterUserDto } from './dto/register-user.dto';
import { UserPayloadDto } from './dto/user-payload.dto';
import { EncoderService } from './encoder.service';
import { UsersRepository } from './users.repository';
import { SpotifyApiService } from 'src/spotify-api/spotify-api.service';
export declare class AuthService {
    private usersRepository;
    private encoderService;
    private jwtService;
    private spotifyService;
    constructor(usersRepository: UsersRepository, encoderService: EncoderService, jwtService: JwtService, spotifyService: SpotifyApiService);
    registerUser(registerUserDto: RegisterUserDto): Promise<void>;
    login(loginDto: LoginDto): Promise<{
        access_token: string;
        userData: {
            username: string;
            id: string;
        };
    }>;
    getUser(userPayloadDto: UserPayloadDto): Promise<{
        access_token: string;
        userData: {
            username: string;
            id: string;
        };
    }>;
}
