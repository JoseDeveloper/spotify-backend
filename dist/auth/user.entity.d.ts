export declare class User {
    id: string;
    username: string;
    email: string;
    spotifyToken: string;
    password: string;
    active: boolean;
    createdOn: Date;
    updatedOn: Date;
}
