export declare class UpdateSpotifyTokenDto {
    id: string;
    spotifyToken: string;
}
