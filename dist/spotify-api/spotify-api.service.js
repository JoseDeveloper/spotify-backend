"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SpotifyApiService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const node_fetch_1 = require("node-fetch");
const users_repository_1 = require("../auth/users.repository");
let SpotifyApiService = class SpotifyApiService {
    async getToken() {
        const btoaSecret = Buffer.from(`${process.env.CLIENT_ID}:${process.env.CLIENT_SECRET}`).toString('base64');
        const fetching = await (0, node_fetch_1.default)(`${process.env.SPOTIFY_URI_TOKEN}token`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                'Authorization': 'Basic ' + btoaSecret
            },
            body: 'grant_type=client_credentials'
        }).then(response => response.json())
            .then(data => {
            return data;
        });
        return fetching.access_token;
    }
    async getArtistAlbums(token, artist, userId) {
        const fetching = await (0, node_fetch_1.default)(`${process.env.SPOTIFY_URI_SEARCH}search?q=artist:${artist}&type=track`, {
            method: "GET",
            headers: {
                Authorization: `Bearer ${token}`
            },
            json: true
        }).then(response => response.json())
            .then(data => {
            return data;
        }).catch(error => {
            return error;
        });
        if (fetching.error) {
            if (fetching.error.status === 401) {
                const token = await this.getToken();
                await this.usersRepository.updateUserSpotifyToken(userId, token);
            }
        }
        else {
            return fetching;
        }
    }
};
__decorate([
    (0, typeorm_1.InjectRepository)(users_repository_1.UsersRepository),
    __metadata("design:type", users_repository_1.UsersRepository)
], SpotifyApiService.prototype, "usersRepository", void 0);
SpotifyApiService = __decorate([
    (0, common_1.Injectable)()
], SpotifyApiService);
exports.SpotifyApiService = SpotifyApiService;
//# sourceMappingURL=spotify-api.service.js.map