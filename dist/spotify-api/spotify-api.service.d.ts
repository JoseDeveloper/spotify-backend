export declare class SpotifyApiService {
    private usersRepository;
    getToken(): Promise<any>;
    getArtistAlbums(token: string, artist: string, userId: any): Promise<any>;
}
