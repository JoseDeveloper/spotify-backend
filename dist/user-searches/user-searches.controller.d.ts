import { Request } from 'express';
import { UserSearchesService } from './user-searches.service';
export declare class UserSearchesController {
    private userSearchesService;
    constructor(userSearchesService: UserSearchesService);
    searchArtistAlbums(req: Request, artist: string, userIp: string): Promise<Object>;
    getByUserId(req: Request): Promise<Object[]>;
}
