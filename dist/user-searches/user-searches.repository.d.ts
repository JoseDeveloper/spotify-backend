import { User } from 'src/auth/user.entity';
import { Repository } from 'typeorm';
import { UserSearches } from './user-searches.entity';
export declare class UserSearchesRepository extends Repository<UserSearches> {
    createUserSearch(userId: User, userIp: string, search: string): Promise<void>;
    updateUserSpotifyToken(id: string, search: string): Promise<any>;
    findOneById(id: string): Promise<UserSearches>;
    findByUserId(userId: string): Promise<UserSearches[]>;
    findByUserAndSearch(userId: any, userIp: any, search: any): Promise<UserSearches>;
}
