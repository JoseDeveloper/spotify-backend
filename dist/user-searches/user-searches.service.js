"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserSearchesService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const users_repository_1 = require("../auth/users.repository");
const process_data_utils_1 = require("../common/utils/process-data.utils");
const spotify_api_service_1 = require("../spotify-api/spotify-api.service");
const user_searches_repository_1 = require("./user-searches.repository");
let UserSearchesService = class UserSearchesService {
    constructor(userSearchesRepository, usersRepository, spotifyService) {
        this.userSearchesRepository = userSearchesRepository;
        this.usersRepository = usersRepository;
        this.spotifyService = spotifyService;
    }
    async getArtistAlbums(artist, user) {
        const id = user.id;
        const { spotifyToken } = await this.usersRepository.findOneById(id);
        const data = await this.spotifyService.getArtistAlbums(spotifyToken, artist, id);
        const dataTracks = data.tracks.items;
        const processedData = (0, process_data_utils_1.processData)(dataTracks);
        const orderedData = (0, process_data_utils_1.sortData)(processedData);
        return orderedData;
    }
    async updateUserSearch(search, userIp, user) {
        const userId = user.id;
        const userSearch = await this.userSearchesRepository.findByUserAndSearch(userId, userIp, search);
        if (userSearch) {
            return userSearch;
        }
        else {
            const userData = await this.usersRepository.findOneById(userId);
            return await this.userSearchesRepository.createUserSearch(userData, userIp, search);
        }
    }
    async getByUserId(user) {
        const userId = user.id;
        const userSearch = await this.userSearchesRepository.findByUserId(userId);
        return userSearch;
    }
};
UserSearchesService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(user_searches_repository_1.UserSearchesRepository)),
    __param(1, (0, typeorm_1.InjectRepository)(users_repository_1.UsersRepository)),
    __metadata("design:paramtypes", [user_searches_repository_1.UserSearchesRepository,
        users_repository_1.UsersRepository,
        spotify_api_service_1.SpotifyApiService])
], UserSearchesService);
exports.UserSearchesService = UserSearchesService;
//# sourceMappingURL=user-searches.service.js.map