"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserSearches = void 0;
const user_entity_1 = require("../auth/user.entity");
const typeorm_1 = require("typeorm");
let UserSearches = class UserSearches {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", String)
], UserSearches.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)({ name: 'search', default: null }),
    __metadata("design:type", String)
], UserSearches.prototype, "search", void 0);
__decorate([
    (0, typeorm_1.Column)({ name: 'user_ip', default: null }),
    __metadata("design:type", String)
], UserSearches.prototype, "userIp", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)({ name: 'date', default: null }),
    __metadata("design:type", Date)
], UserSearches.prototype, "date", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(() => user_entity_1.User, (user) => user.id),
    (0, typeorm_1.JoinColumn)({ name: 'user_id' }),
    __metadata("design:type", user_entity_1.User)
], UserSearches.prototype, "userId", void 0);
UserSearches = __decorate([
    (0, typeorm_1.Entity)()
], UserSearches);
exports.UserSearches = UserSearches;
//# sourceMappingURL=user-searches.entity.js.map