"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserSearchesModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const users_repository_1 = require("../auth/users.repository");
const spotify_api_service_1 = require("../spotify-api/spotify-api.service");
const user_searches_controller_1 = require("./user-searches.controller");
const user_searches_repository_1 = require("./user-searches.repository");
const user_searches_service_1 = require("./user-searches.service");
let UserSearchesModule = class UserSearchesModule {
};
UserSearchesModule = __decorate([
    (0, common_1.Module)({
        imports: [typeorm_1.TypeOrmModule.forFeature([users_repository_1.UsersRepository, user_searches_repository_1.UserSearchesRepository])],
        controllers: [user_searches_controller_1.UserSearchesController],
        providers: [user_searches_service_1.UserSearchesService, spotify_api_service_1.SpotifyApiService]
    })
], UserSearchesModule);
exports.UserSearchesModule = UserSearchesModule;
//# sourceMappingURL=user-searches.module.js.map