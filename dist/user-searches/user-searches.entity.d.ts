import { User } from 'src/auth/user.entity';
export declare class UserSearches {
    id: string;
    search: string;
    userIp: string;
    date: Date;
    userId: User;
}
