import { UsersRepository } from 'src/auth/users.repository';
import { SpotifyApiService } from 'src/spotify-api/spotify-api.service';
import { UserSearchesRepository } from './user-searches.repository';
export declare class UserSearchesService {
    private userSearchesRepository;
    private usersRepository;
    private spotifyService;
    constructor(userSearchesRepository: UserSearchesRepository, usersRepository: UsersRepository, spotifyService: SpotifyApiService);
    getArtistAlbums(artist: string, user: any): Promise<any>;
    updateUserSearch(search: string, userIp: any, user: any): Promise<void | import("./user-searches.entity").UserSearches>;
    getByUserId(user: any): Promise<import("./user-searches.entity").UserSearches[]>;
}
