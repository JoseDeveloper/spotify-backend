"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserSearchesController = void 0;
const common_1 = require("@nestjs/common");
const user_searches_service_1 = require("./user-searches.service");
const passport_1 = require("@nestjs/passport");
let UserSearchesController = class UserSearchesController {
    constructor(userSearchesService) {
        this.userSearchesService = userSearchesService;
    }
    async searchArtistAlbums(req, artist, userIp) {
        const usr = req.user;
        await this.userSearchesService.updateUserSearch(artist, userIp, usr);
        return this.userSearchesService.getArtistAlbums(artist, usr);
    }
    async getByUserId(req) {
        const usr = req.user;
        return this.userSearchesService.getByUserId(usr);
    }
};
__decorate([
    (0, common_1.Get)('/searchArtistAlbums/:artist/:userIp'),
    __param(0, (0, common_1.Req)()),
    __param(1, (0, common_1.Param)('artist')),
    __param(2, (0, common_1.Param)('userIp')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, String, String]),
    __metadata("design:returntype", Promise)
], UserSearchesController.prototype, "searchArtistAlbums", null);
__decorate([
    (0, common_1.Get)('/'),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UserSearchesController.prototype, "getByUserId", null);
UserSearchesController = __decorate([
    (0, common_1.UseGuards)((0, passport_1.AuthGuard)('jwt')),
    (0, common_1.Controller)('api/user-searches'),
    __metadata("design:paramtypes", [user_searches_service_1.UserSearchesService])
], UserSearchesController);
exports.UserSearchesController = UserSearchesController;
//# sourceMappingURL=user-searches.controller.js.map