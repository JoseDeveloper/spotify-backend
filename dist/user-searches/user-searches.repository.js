"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserSearchesRepository = void 0;
const common_1 = require("@nestjs/common");
const user_entity_1 = require("../auth/user.entity");
const typeorm_1 = require("typeorm");
const user_searches_entity_1 = require("./user-searches.entity");
let UserSearchesRepository = class UserSearchesRepository extends typeorm_1.Repository {
    async createUserSearch(userId, userIp, search) {
        const userData = this.create({ userId, userIp, search });
        try {
            await this.save(userData);
        }
        catch (e) {
            if (e.code === 'ER_DUP_ENTRY') {
                throw new common_1.ConflictException('Error in registration');
            }
            throw new common_1.InternalServerErrorException();
        }
    }
    async updateUserSpotifyToken(id, search) {
        try {
            await this.update(id, { search });
        }
        catch (e) {
            if (e.code === 'ER_DUP_ENTRY') {
                throw new common_1.ConflictException('Error updating');
            }
            throw new common_1.InternalServerErrorException();
        }
        const userSearch = await this.findOneById(id);
        return userSearch;
    }
    async findOneById(id) {
        return await this.findOne({ id });
    }
    async findByUserId(userId) {
        return await this.find({ where: { userId } });
    }
    async findByUserAndSearch(userId, userIp, search) {
        return await this.findOne({ where: { userId, userIp, search } });
    }
};
UserSearchesRepository = __decorate([
    (0, typeorm_1.EntityRepository)(user_searches_entity_1.UserSearches)
], UserSearchesRepository);
exports.UserSearchesRepository = UserSearchesRepository;
//# sourceMappingURL=user-searches.repository.js.map