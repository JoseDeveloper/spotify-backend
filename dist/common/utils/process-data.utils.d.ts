export declare const processData: (dataTracks: any) => any[];
export declare const sortData: (arrAlbums: any) => any;
export declare const groupByArtist: (objectArray: any, property: any) => any;
export declare const groupByAlbum: (objectArray: any) => any;
