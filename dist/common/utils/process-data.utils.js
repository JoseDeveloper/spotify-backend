"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.groupByAlbum = exports.groupByArtist = exports.sortData = exports.processData = void 0;
const processData = (dataTracks) => {
    const groupedBy = (0, exports.groupByArtist)(dataTracks, 'name');
    const arrayElements = Object.values(groupedBy);
    const groupedByAlbums = (0, exports.groupByAlbum)(arrayElements[0]);
    let arrAlbums = [];
    for (var key in groupedByAlbums) {
        let sumPopularity = 0;
        const cover = groupedByAlbums[key][0].album.images[0].url;
        const releaseDate = groupedByAlbums[key][0].album.release_date;
        const totalTracks = groupedByAlbums[key][0].album.total_tracks;
        groupedByAlbums[key].map((itm) => {
            sumPopularity += itm.popularity;
        });
        let tmpObj = { albumName: key, cover, releaseDate, totalTracks, popularity: sumPopularity };
        arrAlbums.push(tmpObj);
    }
    return arrAlbums;
};
exports.processData = processData;
const sortData = (arrAlbums) => {
    const sortedByPopularity = arrAlbums.sort((a, b) => {
        return b.popularity - a.popularity;
    });
    return sortedByPopularity;
};
exports.sortData = sortData;
const groupByArtist = (objectArray, property) => {
    return objectArray.reduce(function (acc, obj) {
        var key = obj.artists[0][property];
        if (!acc[key]) {
            acc[key] = [];
        }
        acc[key].push(obj);
        return acc;
    }, {});
};
exports.groupByArtist = groupByArtist;
const groupByAlbum = (objectArray) => {
    return objectArray.reduce(function (acc, obj) {
        var key = obj.album.name;
        if (!acc[key]) {
            acc[key] = [];
        }
        acc[key].push(obj);
        return acc;
    }, {});
};
exports.groupByAlbum = groupByAlbum;
//# sourceMappingURL=process-data.utils.js.map