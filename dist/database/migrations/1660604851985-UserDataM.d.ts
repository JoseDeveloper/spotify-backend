import { MigrationInterface, QueryRunner } from "typeorm";
export declare class UserDataM1660604851985 implements MigrationInterface {
    name: string;
    up(queryRunner: QueryRunner): Promise<void>;
    down(queryRunner: QueryRunner): Promise<void>;
}
