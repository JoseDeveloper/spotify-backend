"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserSearchDate1660756493582 = void 0;
class UserSearchDate1660756493582 {
    constructor() {
        this.name = 'UserSearchDate1660756493582';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`user_searches\` ADD \`date\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`user_searches\` DROP COLUMN \`date\``);
    }
}
exports.UserSearchDate1660756493582 = UserSearchDate1660756493582;
//# sourceMappingURL=1660756493582-UserSearchDate.js.map