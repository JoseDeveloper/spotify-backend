"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserDataSpotifyToken1660606254583 = void 0;
class UserDataSpotifyToken1660606254583 {
    constructor() {
        this.name = 'UserDataSpotifyToken1660606254583';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`user\` CHANGE \`spotify_token\` \`spotify_token\` varchar(255) NULL`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`user\` CHANGE \`spotify_token\` \`spotify_token\` varchar(255) NOT NULL`);
    }
}
exports.UserDataSpotifyToken1660606254583 = UserDataSpotifyToken1660606254583;
//# sourceMappingURL=1660606254583-UserDataSpotifyToken.js.map