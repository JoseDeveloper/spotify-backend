"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserSearchDefaultValue1660776224019 = void 0;
class UserSearchDefaultValue1660776224019 {
    constructor() {
        this.name = 'UserSearchDefaultValue1660776224019';
    }
    async up(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`user_searches\` CHANGE \`search\` \`search\` varchar(255) NULL`);
        await queryRunner.query(`ALTER TABLE \`user_searches\` CHANGE \`user_ip\` \`user_ip\` varchar(255) NULL`);
        await queryRunner.query(`ALTER TABLE \`user_searches\` CHANGE \`date\` \`date\` datetime(6) NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)`);
    }
    async down(queryRunner) {
        await queryRunner.query(`ALTER TABLE \`user_searches\` CHANGE \`date\` \`date\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`user_searches\` CHANGE \`user_ip\` \`user_ip\` varchar(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`user_searches\` CHANGE \`search\` \`search\` varchar(255) NOT NULL`);
    }
}
exports.UserSearchDefaultValue1660776224019 = UserSearchDefaultValue1660776224019;
//# sourceMappingURL=1660776224019-UserSearchDefaultValue.js.map