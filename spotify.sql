-- MySQL dump 10.13  Distrib 8.0.29, for Linux (x86_64)
--
-- Host: localhost    Database: spotify
-- ------------------------------------------------------
-- Server version	8.0.30-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp` bigint NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,1660604851985,'UserDataM1660604851985'),(2,1660606254583,'UserDataSpotifyToken1660606254583'),(3,1660756493582,'UserSearchDate1660756493582'),(4,1660776224019,'UserSearchDefaultValue1660776224019');
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` char(36) NOT NULL,
  `username` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `spotify_token` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `active` tinyint NOT NULL DEFAULT '0',
  `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_e12875dfb3b1d92d7d7c5377e2` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('f612634c-3070-4e70-96cd-f31521d5d1e2','jose','jose123@gmail.com','BQCYOnGwit16b1aJ93Pj66lcI5RdxgfoI0pNhs-EJKWmnEtobyjondvNzt7ftpCL6WI8Mq35wgjtzj5w7P4Z3F6wLiPYilPt5LNc8UbvKOo7n2C5nGM','$2b$10$wRIaTqQ5KWLbvZui4dubDui1LukjpPFWlsKZJLkaOinwIdcSxzjGO',0,'2022-08-15 18:31:15.146685','2022-08-18 18:59:59.000000');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_searches`
--

DROP TABLE IF EXISTS `user_searches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_searches` (
  `id` int NOT NULL AUTO_INCREMENT,
  `search` varchar(255) DEFAULT NULL,
  `user_ip` varchar(255) DEFAULT NULL,
  `user_id` char(36) DEFAULT NULL,
  `date` datetime(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`id`),
  KEY `FK_c5e85a562a77219e953db89c882` (`user_id`),
  CONSTRAINT `FK_c5e85a562a77219e953db89c882` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_searches`
--

LOCK TABLES `user_searches` WRITE;
/*!40000 ALTER TABLE `user_searches` DISABLE KEYS */;
INSERT INTO `user_searches` VALUES (1,'korn',NULL,NULL,'2022-08-17 17:47:20.716848'),(2,'korn','3345.7878.999',NULL,'2022-08-17 17:59:34.242866'),(3,'merauder','3345.7878.999',NULL,'2022-08-17 18:00:03.218154'),(4,'merauder','3345.7878.998',NULL,'2022-08-17 18:00:22.682228'),(5,'korn','3345.7878.998',NULL,'2022-08-17 18:03:09.976390'),(6,'ramstein','3345.7878.998',NULL,'2022-08-17 18:41:11.673117'),(7,'maluma','3345.7878.998',NULL,'2022-08-17 18:45:55.841255'),(8,'maluma','3345.7878.998','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-17 18:55:02.271576'),(9,'slayer','3345.7878.998','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-17 18:56:08.005322'),(10,'maluma','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 08:49:42.841003'),(11,'metallica','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 09:07:29.064971'),(12,'wisin','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 09:07:38.860694'),(13,'rap','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 09:10:14.200458'),(14,'j balvin','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 12:03:28.923071'),(15,'korn','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 13:24:22.331341'),(16,'korn','0','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 13:33:11.077423'),(17,'animals as leaders','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 13:35:50.343211'),(18,'korn','3345.7878.998','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 15:03:56.573750'),(19,'limp bizkit','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 15:23:50.521928'),(20,'exodus','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 15:24:02.672697'),(21,'madonna','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 15:27:19.618215'),(22,'onyx','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 15:32:03.963280'),(23,'tesseract','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 16:07:20.652456'),(24,'liquid tension','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 16:07:31.581245'),(25,'slayer','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 17:39:08.663869'),(26,'daddy yankee','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 17:39:31.850290'),(27,'kreator','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 17:48:44.130891'),(28,'dream theater','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 17:49:52.135196'),(29,'decapitated','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 17:50:09.523200'),(30,'cypress hill','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 17:50:23.628271'),(31,'marilyn manson','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 17:52:16.555213'),(32,'luis miguel','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 17:59:09.837129'),(33,'alicia keys','190.6.49.113','f612634c-3070-4e70-96cd-f31521d5d1e2','2022-08-18 17:59:23.889881');
/*!40000 ALTER TABLE `user_searches` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-08-19  8:26:54
