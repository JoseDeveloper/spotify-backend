import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersRepository } from 'src/auth/users.repository';
import { SpotifyApiService } from './spotify-api.service';

@Module({
  imports: [TypeOrmModule.forFeature([UsersRepository])],
  providers: [SpotifyApiService]
})
export class SpotifyApiModule {}
