import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Console } from 'console';
import fetch from 'node-fetch';
import { UsersRepository } from 'src/auth/users.repository';


@Injectable()
export class SpotifyApiService {
    @InjectRepository(UsersRepository)
    private usersRepository: UsersRepository

    async getToken() {
        const btoaSecret = Buffer.from(`${process.env.CLIENT_ID}:${process.env.CLIENT_SECRET}`).toString('base64');

        const fetching = await fetch(`${process.env.SPOTIFY_URI_TOKEN}token`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
                'Authorization': 'Basic ' + btoaSecret
            },
            body: 'grant_type=client_credentials'
        }).then(response => response.json())
            .then(data => {
                return data;
            });

        return fetching.access_token;

    }

    async getArtistAlbums(token: string, artist: string, userId: any) {

        const fetching = await fetch(`${process.env.SPOTIFY_URI_SEARCH}search?q=artist:${artist}&type=track`, {
            method: "GET",
            headers: {
                Authorization: `Bearer ${token}`
            },
            json: true

        }).then(response => response.json())
            .then(data => {
                return data;
            }).catch(error => {
                return error;
          
            });

        if(fetching.error){
            if(fetching.error.status === 401){
                const token = await this.getToken(); 
                await this.usersRepository.updateUserSpotifyToken(userId, token);
            }
        }else{
            return fetching;
        }

    }
}
