import { Test, TestingModule } from '@nestjs/testing';
import { UserSearchesController } from './user-searches.controller';

describe('UserSearchesController', () => {
  let controller: UserSearchesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserSearchesController],
    }).compile();

    controller = module.get<UserSearchesController>(UserSearchesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
