import { Test, TestingModule } from '@nestjs/testing';
import { UserSearchesService } from './user-searches.service';

describe('UserSearchesService', () => {
  let service: UserSearchesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserSearchesService],
    }).compile();

    service = module.get<UserSearchesService>(UserSearchesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
