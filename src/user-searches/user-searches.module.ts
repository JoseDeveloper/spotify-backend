import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersRepository } from 'src/auth/users.repository';
import { SpotifyApiService } from 'src/spotify-api/spotify-api.service';
import { UserSearchesController } from './user-searches.controller';
import { UserSearchesRepository } from './user-searches.repository';
import { UserSearchesService } from './user-searches.service';

@Module({
  imports: [TypeOrmModule.forFeature([UsersRepository,UserSearchesRepository])],
  controllers: [UserSearchesController],
  providers: [UserSearchesService, SpotifyApiService]
})
export class UserSearchesModule {}
