import {
    ConflictException,
    InternalServerErrorException,
  } from '@nestjs/common';
import { User } from 'src/auth/user.entity';
  import { EntityRepository, Repository } from 'typeorm';
//   import { UpdateSpotifyTokenDto } from './dto/updateSpotifyToken.dto';
  import { UserSearches } from './user-searches.entity';
  
  @EntityRepository(UserSearches)
  export class UserSearchesRepository extends Repository<UserSearches> {
  
    async createUserSearch(userId: User, userIp: string, search: string ): Promise<void> {
      const userData = this.create({ userId, userIp, search });
  
      try {
        await this.save(userData);
      } catch (e) {
        if (e.code === 'ER_DUP_ENTRY') {
          throw new ConflictException('Error in registration');
        }
        throw new InternalServerErrorException();
      }
    }
  
    async updateUserSpotifyToken(id: string, search: string): Promise<any> {
        try {
           await this.update(id, {search});
          
        } catch (e) { 
          if (e.code === 'ER_DUP_ENTRY') {
            throw new ConflictException('Error updating');
          }
          throw new InternalServerErrorException();
        }
  
  
      const userSearch = await this.findOneById(id);
      return userSearch;
  
    }

    async findOneById(id: string): Promise<UserSearches> {
      return await this.findOne({ id });
    }

    async findByUserId(userId: string): Promise<UserSearches[]>{
      return await this.find({ where:  { userId } }); 
    }

    async findByUserAndSearch(userId, userIp, search): Promise<UserSearches> {

        return await this.findOne({ where:  { userId, userIp, search } });  
    }
  }
  