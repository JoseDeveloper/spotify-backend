import { User } from 'src/auth/user.entity';
import {
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
  ManyToOne,
} from 'typeorm';

// import { SpaceType } from 'src/space-type/space-type.entity'; 

@Entity()
export class UserSearches {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({name: 'search', default: null})
  search: string;

  @Column({name: 'user_ip', default: null})
  userIp: string;

  @UpdateDateColumn({name:'date', default: null})
  date: Date;

  @ManyToOne(() => User, (user) => user.id)
  @JoinColumn({ name: 'user_id' })
  public userId!: User;

}