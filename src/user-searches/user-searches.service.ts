import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UsersRepository } from 'src/auth/users.repository';
import { processData, sortData } from 'src/common/utils/process-data.utils';
import { SpotifyApiService } from 'src/spotify-api/spotify-api.service';
import { UserSearchesRepository } from './user-searches.repository';

@Injectable()
export class UserSearchesService {
    constructor(
        @InjectRepository(UserSearchesRepository)
        private userSearchesRepository: UserSearchesRepository,

        @InjectRepository(UsersRepository)
        private usersRepository: UsersRepository,
        private spotifyService: SpotifyApiService
    ) { }

    async getArtistAlbums(artist: string, user: any) {

        const id = user.id;
        const { spotifyToken } = await this.usersRepository.findOneById(id);

        const data = await this.spotifyService.getArtistAlbums(spotifyToken, artist, id);

        const dataTracks = data.tracks.items;
        const processedData = processData(dataTracks);
        const orderedData = sortData(processedData);
        return orderedData;

    }

    
    async updateUserSearch(search: string, userIp: any, user: any) {

        const userId = user.id;
        const userSearch = await this.userSearchesRepository.findByUserAndSearch(userId, userIp, search)

        if (userSearch) {
            return userSearch;
        } else {
            const userData = await this.usersRepository.findOneById(userId);
            return await this.userSearchesRepository.createUserSearch(userData, userIp, search);
        }

    }

    async getByUserId(user: any){

        const userId = user.id;
        const userSearch = await this.userSearchesRepository.findByUserId(userId);

        return userSearch;

    }

}
