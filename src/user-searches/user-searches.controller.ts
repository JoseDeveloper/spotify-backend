import { Controller, Get, Param, Post, Req, UseGuards } from '@nestjs/common';
import { Request } from 'express';
import { UserSearchesService } from './user-searches.service';
import {AuthGuard} from '@nestjs/passport';

@UseGuards(AuthGuard('jwt'))
@Controller('api/user-searches')
export class UserSearchesController {

    constructor(private userSearchesService: UserSearchesService) {}

    @Get('/searchArtistAlbums/:artist/:userIp')
    async searchArtistAlbums(@Req() req: Request, @Param('artist') artist: string, @Param('userIp') userIp: string): Promise<Object> {
        const usr = req.user;
        await this.userSearchesService.updateUserSearch(artist, userIp, usr);
        return this.userSearchesService.getArtistAlbums(artist, usr);
    }

    @Get('/')
    async getByUserId(@Req() req: Request): Promise<Object[]> {
        const usr = req.user;
        return this.userSearchesService.getByUserId(usr);
    }

}
