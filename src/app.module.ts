import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { DatabaseModule } from './database/database.module';
import { UserSearchesModule } from './user-searches/user-searches.module';
import { SpotifyApiModule } from './spotify-api/spotify-api.module';
@Module({
  imports: [
    DatabaseModule,
    AuthModule,
    UserSearchesModule,
    SpotifyApiModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
