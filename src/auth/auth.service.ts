import { HttpService } from '@nestjs/axios';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { LoginDto } from './dto/login.dto';
import { RegisterUserDto } from './dto/register-user.dto';
import { UserPayloadDto } from './dto/user-payload.dto';
import { EncoderService } from './encoder.service';
import { User } from './user.entity';
import { UsersRepository } from './users.repository';
import fetch from 'node-fetch';
import { JwtPayload } from './jwt-payload.interface';
import { SpotifyApiService } from 'src/spotify-api/spotify-api.service';


@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(UsersRepository)
    private usersRepository: UsersRepository,
    private encoderService: EncoderService,
    private jwtService: JwtService,
    private spotifyService: SpotifyApiService
  ) { }

  async registerUser(registerUserDto: RegisterUserDto): Promise<void> {
    const { username, email, password } = registerUserDto;
    const hashedPassword = await this.encoderService.encodePassword(password);
    return this.usersRepository.createUser(username, email, hashedPassword);
  }

  async login(loginDto: LoginDto) {
    const { email, password } = loginDto;
    const user = await this.usersRepository.findOneByEmail(email);

    if (
      user && (await this.encoderService.checkPassword(password, user.password))
    ) {

      const token = await this.spotifyService.getToken();

      await this.usersRepository.updateUserSpotifyToken(user.id, token);

      const payload: JwtPayload = { id: user.id, email, active: user.active };
      const accessToken = await this.jwtService.sign(payload);
      const { username, id } = user;
      return { access_token: accessToken, userData: {username, id }};
    }

    throw new UnauthorizedException('Please check your credentials');
  }

  async getUser(userPayloadDto: UserPayloadDto) {
    const user = await this.usersRepository.findOneByEmail(userPayloadDto.email);
    const token = await this.spotifyService.getToken();

      await this.usersRepository.updateUserSpotifyToken(user.id, token);

      const payload: JwtPayload = { id: user.id, email: userPayloadDto.email, active: user.active };
      const accessToken = await this.jwtService.sign(payload);
      const { username, id } = user;
      return { access_token: accessToken, userData: {username, id }};
      
  }
}
