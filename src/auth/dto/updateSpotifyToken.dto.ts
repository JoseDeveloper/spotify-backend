import { IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';

export class UpdateSpotifyTokenDto {
  @IsNotEmpty()
  id: string;

  @IsNotEmpty()
  spotifyToken: string;
  
}