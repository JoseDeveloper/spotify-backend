import { IsNotEmpty, IsEmail, Length } from 'class-validator';

export class UserPayloadDto {
  @IsNotEmpty()
  @IsEmail()
  email: string;

}
