import {
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { UpdateSpotifyTokenDto } from './dto/updateSpotifyToken.dto';
import { User } from './user.entity';

@EntityRepository(User)
export class UsersRepository extends Repository<User> {

  async createUser(username: string, email: string, password: string ): Promise<void> {
    const user = this.create({ username, email, password });

    try {
      await this.save(user);
    } catch (e) {
      if (e.code === 'ER_DUP_ENTRY') {
        throw new ConflictException('Error in registration');
      }
      throw new InternalServerErrorException();
    }
  }

  async updateUserSpotifyToken(id: string, token: string): Promise<any> {

      try {
         await this.update(id, {spotifyToken: token.toString()});
        
      } catch (e) { 

        if (e.code === 'ER_DUP_ENTRY') {
          throw new ConflictException('Error saving user data');
        }
        throw new InternalServerErrorException();
      }


    const user = await this.findOneById(id);
    return user;

  }

  async findOneByEmail(email: string): Promise<User> {
    return await this.findOne({ email });
  }

  async findOneById(id: string): Promise<User> {
    return await this.findOne({ id });
  }

  async findByPayload({ email }: any): Promise<User> {
    return await this.findOne({ 
        where:  { email } });  
  }

}
