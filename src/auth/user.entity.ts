import {
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ length: 20 })
  username: string;

  @Column({ length: 100, unique: true })
  email: string;

  @Column({name:'spotify_token', default: null})
  spotifyToken: string;

  @Column()
  password: string;

  @Column({ type: 'boolean', default: false })
  active: boolean;

  @CreateDateColumn({name:'created_at'})
  createdOn: Date;

  @UpdateDateColumn({name:'updated_at'})
  updatedOn: Date;

  // @OneToMany(() => UserSearches, (userSearches) => userSearches.userId)
  // public userSearches!: UserSearches[];
  
}
