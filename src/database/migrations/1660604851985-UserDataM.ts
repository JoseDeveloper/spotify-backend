import {MigrationInterface, QueryRunner} from "typeorm";

export class UserDataM1660604851985 implements MigrationInterface {
    name = 'UserDataM1660604851985'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`user_searches\` (\`id\` int NOT NULL AUTO_INCREMENT, \`search\` varchar(255) NOT NULL, \`user_ip\` varchar(255) NOT NULL, \`user_id\` char(36) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`user\` (\`id\` char(36) NOT NULL, \`username\` varchar(20) NOT NULL, \`email\` varchar(100) NOT NULL, \`spotify_token\` varchar(255) NOT NULL, \`password\` varchar(255) NOT NULL, \`active\` tinyint NOT NULL DEFAULT 0, \`created_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), UNIQUE INDEX \`IDX_e12875dfb3b1d92d7d7c5377e2\` (\`email\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`user_searches\` ADD CONSTRAINT \`FK_c5e85a562a77219e953db89c882\` FOREIGN KEY (\`user_id\`) REFERENCES \`user\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user_searches\` DROP FOREIGN KEY \`FK_c5e85a562a77219e953db89c882\``);
        await queryRunner.query(`DROP INDEX \`IDX_e12875dfb3b1d92d7d7c5377e2\` ON \`user\``);
        await queryRunner.query(`DROP TABLE \`user\``);
        await queryRunner.query(`DROP TABLE \`user_searches\``);
    }

}
