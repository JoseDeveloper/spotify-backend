import {MigrationInterface, QueryRunner} from "typeorm";

export class UserDataSpotifyToken1660606254583 implements MigrationInterface {
    name = 'UserDataSpotifyToken1660606254583'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user\` CHANGE \`spotify_token\` \`spotify_token\` varchar(255) NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user\` CHANGE \`spotify_token\` \`spotify_token\` varchar(255) NOT NULL`);
    }

}
