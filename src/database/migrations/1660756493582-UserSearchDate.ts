import {MigrationInterface, QueryRunner} from "typeorm";

export class UserSearchDate1660756493582 implements MigrationInterface {
    name = 'UserSearchDate1660756493582'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user_searches\` ADD \`date\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user_searches\` DROP COLUMN \`date\``);
    }

}
