import {MigrationInterface, QueryRunner} from "typeorm";

export class UserSearchDefaultValue1660776224019 implements MigrationInterface {
    name = 'UserSearchDefaultValue1660776224019'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user_searches\` CHANGE \`search\` \`search\` varchar(255) NULL`);
        await queryRunner.query(`ALTER TABLE \`user_searches\` CHANGE \`user_ip\` \`user_ip\` varchar(255) NULL`);
        await queryRunner.query(`ALTER TABLE \`user_searches\` CHANGE \`date\` \`date\` datetime(6) NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user_searches\` CHANGE \`date\` \`date\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`user_searches\` CHANGE \`user_ip\` \`user_ip\` varchar(255) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`user_searches\` CHANGE \`search\` \`search\` varchar(255) NOT NULL`);
    }

}
