import { DynamicModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

export const DatabaseProvider: DynamicModule = TypeOrmModule.forRootAsync({
  useFactory: () => ({
    type: 'mysql',
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT) || 3306,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    synchronize: false,
    migrationsRun: true,
    logging: true,
    entities: ["dist/**/*.entity{.ts,.js}"],
    migrations: [__dirname + '/dist/migrations/**/*{.ts,.js}'],
    cli: {
      migrationsDir: './dist/database/migrations',
    },
    keepConnectionAlive: true,
    autoLoadEntities: true

  })
});
