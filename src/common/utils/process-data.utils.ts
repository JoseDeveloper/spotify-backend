
export const processData = (dataTracks) => {
    const groupedBy = groupByArtist(dataTracks, 'name');
    const arrayElements = Object.values(groupedBy);
    const groupedByAlbums = groupByAlbum(arrayElements[0]);
    let arrAlbums = [];

    for (var key in groupedByAlbums) {
        let sumPopularity = 0;

        const cover = groupedByAlbums[key][0].album.images[0].url;
        const releaseDate = groupedByAlbums[key][0].album.release_date;
        const totalTracks = groupedByAlbums[key][0].album.total_tracks;

        groupedByAlbums[key].map((itm) => {
            sumPopularity += itm.popularity;
        });

        let tmpObj = { albumName: key, cover, releaseDate, totalTracks, popularity: sumPopularity }
        arrAlbums.push(tmpObj)
    }

    return arrAlbums;
}

export const sortData = (arrAlbums) => {
    const sortedByPopularity = arrAlbums.sort((a, b) => {
        return b.popularity - a.popularity;
    });

    return sortedByPopularity;
}

export const groupByArtist = (objectArray, property) => {
    return objectArray.reduce(function (acc, obj) {
        var key = obj.artists[0][property];
        if (!acc[key]) {
            acc[key] = [];
        }

        acc[key].push(obj);
        return acc;
    }, {})
}

export const groupByAlbum = (objectArray) => {
    return objectArray.reduce(function (acc, obj) {
        var key = obj.album.name;
        if (!acc[key]) {
            acc[key] = [];
        }

        acc[key].push(obj);
        return acc;
    }, {})
}